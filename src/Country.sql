USE classicmodels;
SELECT country, quantityOrdered, priceEach
FROM customers
JOIN orders
ON customers.customerNumber = orders.customerNumber
JOIN orderdetails
ON orders.orderNumber = orderdetails.orderNumber
;